import sys
import os
import pprint
import configparser
import argparse
import gitlab
from pathlib import Path

def init_gitlab():
    '''Initiate the GitLab object with your gitlab configs

    :return: GitLab instance
    '''
    cfgfile = os.path.join(str(Path.home()), '.python-gitlab.cfg')
    if not os.path.isfile(cfgfile):
        raise FileNotFoundError
    cp = configparser.ConfigParser()
    mycfg = cp.read(cfgfile)
    try:
        toreturn = gitlab.Gitlab(cp['com']['url'], private_token=cp['com']['private_token'])
    except KeyError as e:
        raise e
    return toreturn



def parse_args(args):
    """
    Parse command line arguments

    :param args: command args

    :returns: argparse args
    """
    parser = argparse.ArgumentParser(description="List users in GitLab")
    group = parser.add_mutually_exclusive_group(required=True)
    parser.add_argument('--feature', dest='feature', action='store_true')
    group.add_argument('--pid', type=int, help='The ID of the project')
    group.add_argument('--gid', type=int, help='The ID of the group')
    parser.add_argument('--recursive', dest='recursive', action='store_true', default=False, help='Do you want to search recursive?')
    pargs = parser.parse_args(args)
    return pargs

def main(**kwargs):

    gl = init_gitlab()

    pid = kwargs.get('pid')
    gid = kwargs.get('gid')
    recursive = kwargs.get('recursive')

    if gid:
        thegroup = gl.groups.get(gid)
        print("GROUP => {}".format(thegroup.name))
        if recursive:
            members = thegroup.members.all(all=recursive)
        else:
            members = thegroup.members.list()
    elif pid:
        theproject = gl.projects.get(pid)
        print("PROJECT => {}".format(theproject.name))
        if recursive:
            members = theproject.members.all(all=recursive)
        else:
            members = theproject.members.list()

    print("-"*30)
    member_usernames = '","'.join(m.username for m in members)
    print('"' + member_usernames + '"')
    print("-"*30)

    member_names = '","'.join(m.name for m in members)
    print('"' + member_names + '"')

    
if __name__ == "__main__":
    args = parse_args(sys.argv[1:])
    sys.exit(main(**vars(args)))
