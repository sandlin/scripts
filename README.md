# GitLab API Usage

- This project will be to demonstrate using Python to access the GitLab API
- The key libraries used by this are:
  - [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/)
  - [GitLab API](https://docs.gitlab.com/ee/api/api_resources.html)

---
## System Setup
> NOTE: The instructions below are focused on MacOS.

#### GitLab OAuth Token

1. Setup your auth token @ https://gitlab.com/profile/personal_access_tokens
   1. Grant your token the following Scopes: `api`
   1. Copy the token generated as it will not be accessible once you leave the page
1. Create your python-gitlab auth / config file `~/.python-gitlab.cfg`
   1. Replace the private_token value with the value received above.
   1. Your file will look like:
        ```shell script
        # REF: https://python-gitlab.readthedocs.io/en/stable/cli.html#configuration
        [global]
        default = com
        ssl_verify = true
        timeout = 5

        [com]
        url = https://gitlab.com
        private_token = xxxxxxxxxxx
        api_version = 4
        ```

#### Python Environment

- **Note:** This repo uses Pipenv but you can use what you want for dependency management.
- **Note:** This instruction set is written for MacOS. If you are on Linux, the only items which will differ is how you install `pipenv` and the location of your home directory.

1. Ensure you have `Python 3.8` installed
    ```shell script
    ➜ python3 --version
    Python 3.8.6
    ```
1. Install `pipenv`.
    ```
    ➜ brew install pipenv
   ...
    ```
1. Make certain you are in the `gitlab_api` clone directory.
    ```shell script
    ➜ pwd
    /Users/hulko/workspaces/gitlab_api
    ```
1. Initialize your `pipenv`
    ```shell script
    ➜ pipenv install
    Creating a virtualenv for this project…
    ...
    To activate this project's virtualenv, run pipenv shell.
    Alternatively, run a command inside the virtualenv with pipenv run.
    ```
1. Activate your project's virtualenv
    ```shell script
    ➜ pipenv shell
    Launching subshell in virtual environment…
    . /Users/hulko/.local/share/virtualenvs/gitlab_api-Q0PixHAH/bin/activate
    ```
1. Try a test run
   ```
   ➜ python ./gitlab_userlist.py --gid 7117794
   GROUP => sandlin
   ------------------------------
   "jgsandlin"
   ------------------------------
   "James Sandlin"
   ```
---
## Args
You must include either `pid` or `gid`. `recursive` is optional
- `--pid <YOUR PROJECT ID>`: The project ID you would like to query.
- `--gid <YOUR GROUP ID>`: The group ID you would like to query.
- `--recursive`: Include members recursively (including inherited members through ancestor groups).

---
## Scripts
### project_empty_container_registry.py
- Role: Delete all containers from specified project. 
- Use Case: In GitLab, you cannot move or rename a project which contains Docker images. I want to move a project from one subgroup to another. Therefore, I must delete all Docker images first.
- Args:
  - `--pid <YOUR PROJECT ID>`: The project ID you would like to empty.
   

```
#### gitlab_userlist.py
```
➜ python ./gitlab_userlist.py --pid 278964
PROJECT => GitLab
------------------------------
"Haydn","tmaczukin","xyzzy","annabeldunstone","sarahj","ClemMakesApps","jameslopez","twk3","MadLittleMods","erica","mrogge","pauldalmeida","marcia","timzallmann","klawrence","julie_manalo","mbell","jamedjo","mikegreiling","gitlab-crowdin-bot"
------------------------------
"Haydn Mackay","Tomasz Maczukin","Reb","Annabel Dunstone Gray","Sarah Jones","Clement Ho","James Lopez","DJ Mountney","Eric Eastwood","Erica ","Mark Rogge","Paul Almeida","Marcia Ramos","Tim Zallmann","Kristen","Julie Manalo","Mark Bell","James Edwards-Jones","Mike Greiling","GitLab Crowdin Bot"

```

```
➜ python ./gitlab_userlist.py --gid 7117794
GROUP => sandlin
------------------------------
"jgsandlin"
------------------------------
"James Sandlin"
```

```
➜ python ./gitlab_userlist.py --pid 278964 --recursive
...                                                                                         
```