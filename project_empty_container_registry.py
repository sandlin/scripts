import os
import sys
import argparse
import configparser
import gitlab
from pathlib import Path



def init_gitlab():
    """
    Initiate the GitLab object with your gitlab configs

    :return: GitLab instance
    """
    cfgfile = os.path.join(str(Path.home()), '.python-gitlab.cfg')
    if not os.path.isfile(cfgfile):
        raise FileNotFoundError
    cp = configparser.ConfigParser()
    mycfg = cp.read(cfgfile)
    try:
        toreturn = gitlab.Gitlab(cp['com']['url'], private_token=cp['com']['private_token'])
    except KeyError as e:
        raise e
    return toreturn

    

def parse_args(args):
    """
    Parse command line arguments

    :param args: command args

    :returns: argparse args
    """
    parser = argparse.ArgumentParser(description="Delete all containers in repo's registry")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--pid', type=int, help='The ID of the project')
    pargs = parser.parse_args(args)
    return pargs




def main(**kwargs):
    """
    Delete all docker images published in the project

    :param kwargs: pid - The project ID which you'd like to clear
    :return: None
    """
    pid = kwargs.get('pid')
    
    gl = init_gitlab()

    theproject = gl.projects.get(pid)
    print("PROJECT => {}".format(theproject.name))
    # CLI version of this list command:
    #    gitlab project-registry-repository list --project-id 25562320
    for r in theproject.repositories.list():
        theproject.repositories.delete(r.id)
        print("{} has been deleted".format(r.id))


if __name__ == "__main__":
    args = parse_args(sys.argv[1:])
    sys.exit(main(**vars(args)))
