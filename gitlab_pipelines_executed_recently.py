import sys
import csv
from objects import User
import pprint
import argparse
from operator import attrgetter
import pytz
from datetime import datetime, timedelta
import dateutil.parser
import os
import configparser
from pathlib import Path
import gitlab

def init_gitlab():
    '''Initiate the GitLab object with your gitlab configs

    :return: GitLab instance
    '''
    cfgfile = os.path.join(str(Path.home()), '.python-gitlab.cfg')
    if not os.path.isfile(cfgfile):
        raise FileNotFoundError
    cp = configparser.ConfigParser()
    mycfg = cp.read(cfgfile)
    try:
        toreturn = gitlab.Gitlab(cp['com']['url'], private_token=cp['com']['private_token'])
    except KeyError as e:
        raise e
    return toreturn


def load_projects(gitlab, days_inactive):
    projects = gitlab.projects.list(order_by='last_activity_at', as_list=False)
    counter = 0
    #projects = gitlab.projects.list()

    with open('pipelines_executed_report.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerow(["name", "last_activity_at", "web_url"])

        for p in projects:
            #print("-"*30)
            if p.empty_repo:
                continue
            if not p.jobs_enabled:
                continue
            #print(p.name)
            cutoff_date = datetime.now() - timedelta(days=days_inactive)
            cutoff_date = pytz.UTC.localize(cutoff_date)
            if dateutil.parser.parse(p.last_activity_at) < cutoff_date:
                #print("     - {} - {}".format(counter, p.web_url))
                counter = counter + 1
                continue
            else:
                #pprint.pprint("  {}".format(p.web_url))
                #pprint.pprint("  {}".format(p.last_activity_at))
                writer.writerow([p.name, p.last_activity_at, p.web_url])

    print("userreport.csv created")

def main(**kwargs):

    gl = init_gitlab()

    load_projects(gl, 30)
    user_objects = []
    # qlist = gl.users.list(all=True)
    # #qlist = gl.users.list()
    # for u in qlist:
    #     user_objects.append(User(u))
    #################
    # sorted_list = sorted(user_objects, key=attrgetter('last_sign_in_at'))
    # If you want to print dicts:
    # for su in sorted_list:
    #     print(su.__dict__)
    #################
    # To generate CSV:
    # with open('userreport.csv', 'w') as f:
    #     writer = csv.writer(f)
    #     writer.writerow(user_objects[0].get_field_csv())
    #     for u in user_objects:
    #         writer.writerow(u.to_csv())
    # print("userreport.csv created")
if __name__ == "__main__":
    sys.exit(main())
